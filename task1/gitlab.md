# GITLAB
![Gitlab logo](gitlab.png)
## GIT
Git es de código abierto. Git es un software de control de versiones utilizado principalmente en el desarrollo de software, permite a los desarrolladores colaborar en proyectos, realizar seguimiento de las versiones de código, revertir cambios no deseados y coordinar el trabajo en equipo eficientemente.
## GITLAB
Gitlab es una plataforma de desarrollo de software que proporciona un sistema de control de versiones basado en GIT, Gitlab incluye características como repositorios de código, seguimiento de problemas, integración continua (CI), entrega continua (CD).
Además de la version de codigo abierto de GITLAB que se puede instalar en servidores locales, Gitlab ofrece una version en la nube que permite a los equipos colaborar en diferentes proyectos.
## GITLAB CI/CD
* Integración continua (CI): Con Gitlab CI cada vez que se realiza un cambio en el repositorio de código, se desencadena automaticamente un proceso de compilacion y prueba para garantizar que el código nuevo o modificado no cause problemas en la aplicación.
* Entrega continua (CD): COn Gitlab CD se encarga de la entrega de software a un entorno de prueba o producción una vez que ha pasado las pruebas de integración continua, esto permite a los equipos desplegar cambios en la aplicación de forma rápida y segura.

## PIPELINE
pipeline es una serie de pasos automatizados que se ejecutan cada vez que realizas un cambio en tu repositorio de código. Estos pasos pueden incluir compilación, pruebas, análisis de código, despliegue y más. Los pipelines de GitLab están definidos en un archivo llamado .gitlab-ci.yml en la raíz de tu repositorio, y están configurados utilizando YAML (YAML Ain't Markup Language).

La estructura básica de un archivo de definición de pipeline en GitLab se ve así:

```yaml
stages:
  - stage1
  - stage2
  - stage3

job1:
  stage: stage1
  script:
    - comandos1

job2:
  stage: stage2
  script:
    - comandos2

job3:
  stage: stage3
  script:
    - comandos3

```

* **stages** Define los diferentes pasos o etapas del pipeline.
* **job1, job2, job3** Son los trabajos que se ejecutan en cada etapa del pipeline.
* **stage** Indica a qué etapa pertenece cada trabajo.
* **script** Contiene los comandos que se ejecutarán como parte del trabajo.

![gitlab_cicd](gitlab_cicd.png)

Los pipelines en GitLab se activan automáticamente cuando haces un push de cambios a tu repositorio. Cada cambio desencadena la ejecución del pipeline, que ejecutará los pasos definidos en el archivo **.gitlab-ci.yml** en el orden especificado.

## REQUISITOS DE HARDWARE
* **ALMACENAMIENTO**: Al menos debe tener libre como el que ocupan todos sus repositorios combinados.
* **CPU**: los requisitos dependen de la cantidad de usuarios y de la carga de trabajo esperada. 4 núcleos es el número mínimo recomendado.
* **MEMORIA RAM**: 4 GB de RAM es el tamaño de memoria mínimo requerido.
## REQUISITOS
* RUBY
* POSTGRESQL
* REDIS
* GIT
## INSTALACION GITLAB
Para la instalación podemos seguir la [documentación](https://about.gitlab.com/install/#debian) oficial de Gitlab.

1. Instalamos y configuramos las dependencias necesarias.
```bash
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates perl
```
A continuación instalamos Postfix para enviar correos electrónicos de notificación.
```bash
sudo apt-get install -y postfix
```
Durante la instalación de Postfix puede aparecer una pantalla de configuración. Seleccionamos "sitio de internet" y continuamos.

2. Agregamos el repositorio de paquetes de Gitlab e instalamos el paquete.
``` bash
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
```
3. A continuación, instalamos el paquete GitLab donde se configurara automatimente la url como **https://gitlab.example.com**

```bash
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
# List available versions: apt-cache madison gitlab-ee
# Specifiy version: sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee=16.2.3-ee.0
# Pin the version to limit auto-updates: sudo apt-mark hold gitlab-ee
# Show what packages are held back: sudo apt-mark showhold
```

Una vez finalizada la instalacion nos muestre el siguiente mensaje:

![instalacion](2024-02-24_22-24-45.png)

Tambien generará el usuario root con su contraseña generada automaticamente y en que archivo se encuentra.

![contraseña](2024-02-25_21-04-53.png)

Ingresamos al archivo **/etc/gitlab/initial_root_password** y copiamos la contraseña generada para posteriormente ingresar por primera vez a gitlab.

```bash
sudo nano /etc/gitlab/initial_root_password
```

![contraseña](2024-02-24_19-26-36.png)

Luego necesitamos configurar el url de gitlab para poder acceder a traves del navegador, para esto modificamos la siguiente linea **external_url 'https://gitlab.example.com'** en el archivo **/etc/gitlab/gitlab.rb** por la siguiente url **'http:localhost:8083'**

```bash
sudo nano /etc/gitlab/gitlab.rb
```

![URL](2024-02-24_19-33-12.png)

FInalmente ejecutamos los comandos el siguiente comando para guardar las configuraciones realizadas.

```bash
sudo gitlab-ctl reconfigure
```

Y para verificar el estado de gitlab ejecutamos el siguiente comando:

```bash
sudo gitlab-ctl status
```

Luego nos muestra el estado de las dependencias en RUN

![verificacion](2024-02-25_21-21-06.png)

Ingresamos a GITLAB por primera vez en nuestro navegador.

![ingreso](2024-02-24_21-20-37.png)


